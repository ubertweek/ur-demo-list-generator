import psycopg2, json

list_names = ['TV Dramas','New Releases', 'Crime Movies', 'Independent Movies', 'Heartfelt Movies','Kids TV','Children & Family Movies','Action Thrillers']

recommendations = {"recommendations": []}

def pg_connection(database_name, user = 'postgres', host= 'localhost'):
    return psycopg2.connect("dbname={} user='{}' host='{}'".format(database_name, user, host))

def pg_cursor(connection):
    return connection.cursor()

def pg_query(cursor, query):
    cursor.execute(query)

def show_result(cursor):
    rows = cursor.fetchall()
    for row in rows:
        print(row)

def tmdb_objects(cursor):
    rows = cursor.fetchall()
    return {row[0]: row[1] for row in rows}


def query(limit, offset):
    return "SELECT id, details FROM external_objects WHERE tweek_object_id IS NOT NULL AND origin= 'tmdb' LIMIT {} OFFSET {}".format(limit,offset)

connection = pg_connection('database_middleware_production')
cursor = pg_cursor(connection)

object_per_list = 10

for list_n in range(0,8):
    objects = []
    pg_query(cursor, query(object_per_list,list_n * object_per_list))
    tmdb_dict = tmdb_objects(cursor)
    for obj in tmdb_dict.items():
        objects.append({"id": obj[0], "details": obj[1]})
    recommendations['recommendations'].append({"list_name":list_names[list_n], "objects": objects})


#print(json.dumps(recommendations))

with open('recommendations.txt', 'w') as outfile:
    json.dump(recommendations, outfile)
